import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public abstract class AbstractFunctionProcessor implements Parser.IProcessor {

    private String typeStr = "";

    protected AbstractFunctionProcessor(String typeStr) {
        this.typeStr = typeStr;
    }

    protected abstract boolean shouldIgnore(List<String> lines);

    protected void appendToFile(String fileName, List<String> lines) throws IOException {
        int i = lines.size();

        removeSearchPath(lines, i - 1);
        removeSearchPath(lines, i - 2);
        if( i > 4 ) removeSearchPath(lines, i - 3);
        if( i > 5 ) removeSearchPath(lines, i - 4);
        if( i > 6 ) removeSearchPath(lines, i - 5);

        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, true));) {

            for (String line : lines) {
                bufferedWriter.write(line);

                bufferedWriter.newLine();
            }

            bufferedWriter.flush();
        }
    }

    protected File adjustPath(Parser.Item item) throws Exception {
        File schemapath = new File(Parser.Item.rootPath + "/" + item.schemaStr.toLowerCase());
        File typepath = new File(Parser.Item.rootPath + "/" + item.schemaStr + "/" + item.typeStr.toLowerCase());

        if (!schemapath.exists())
            schemapath.mkdir();

        if (!typepath.exists())
            typepath.mkdir();

        if (schemapath.isDirectory() && typepath.isDirectory()) {
            String functionName = item.nameStr.split("[(]")[0].toLowerCase();
            File functionPath = new File(typepath + "/" + functionName + ".sql");

            if (!functionPath.exists())
                functionPath.createNewFile();

            return functionPath;
        } else
            throw new Exception("Erro ao criar a pasta: " + typepath);
    }

    protected void removeSearchPath(List<String> lines, int i) {
        String line = lines.get(i);

        if (line.length() > 17 && line.substring(0, 17).equalsIgnoreCase("SET search_path ="))
            lines.set(i, "");
        else if (line.length() > 24 && line.substring(0, 24).equalsIgnoreCase("SET default_tablespace ="))
            lines.set(i, "");
    }

    protected void changeFromBegin(List<String> lines, String schemaStr, String oldStr, String newStr) {
        try {
            int i = searchLineFromBegin(lines, oldStr);

            if (i > 0)

                if (lines.get(i).startsWith(oldStr)) {
                    String alterFunction = lines.get(i).replace(oldStr, newStr + schemaStr + ".");
                    lines.set(i, alterFunction);
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void changeInMiddle(List<String> lines, String schemaStr, String oldStr, String newStr) {
        try {
            int i = searchLineContains(lines, oldStr);

            if (i > 0)

                if (lines.get(i).contains(oldStr)) {
                    String alterFunction = lines.get(i).replace(oldStr, newStr + schemaStr + ".");
                    lines.set(i, alterFunction);
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void changeFromEnd(List<String> lines, String schemaStr, String oldStr, String newStr) {
        try {
            int i = searchLineFromEnd(lines, oldStr);

            if (i > 0)

                if (lines.get(i).startsWith(oldStr)) {
                    String alterFunction = lines.get(i).replace(oldStr, newStr + schemaStr + ".");
                    alterFunction = alterFunction.replace(schemaStr + "." + schemaStr + ".", schemaStr + ".");
                    lines.set(i, alterFunction);
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void extractToFile(List<String> lines, Parser.Item item) throws Exception {
        File path = adjustPath(item);

        appendToFile(path.toString(), lines);
    }

    protected int searchLineFromBegin(List<String> lines, String startsWith) {
        for (int i = 0; i < lines.size(); i++)
            if (lines.get(i).startsWith(startsWith))
                return i;

        return -1;
    }

    protected int searchLineContains(List<String> lines, String startsWith) {
        for (int i = 0; i < lines.size(); i++)
            if (lines.get(i).contains(startsWith))
                return i;

        return -1;
    }

    /**
     * Procura a partir do final da lista de linha se alguma linha começa com
     * determinada string
     * 
     * @param lines
     *            a lista de linhas
     * @param startsWith
     *            a string de busca
     * @return a posição da linha que começa com a string, ou -1
     */
    protected int searchLineFromEnd(List<String> lines, String startsWith) {
        for (int i = lines.size() - 1; i >= 0; i--)
            if (lines.get(i).startsWith(startsWith))
                return i;

        return -1;
    }

    public String doProcess(List<String> lines, Parser.Item item) {
        if (!shouldIgnore(lines)) {
            // -- System.out.println( lines.toString() );

            try {
                adjustCode(lines, item);

                extractToFile(lines, item);

                return null;
            } catch (Exception e) {
                return "Erro processando " + item.typeStr + " " + item.nameStr + ": " + e.getMessage();
            }
        }

        return null;
    }

    protected abstract void adjustCode(List<String> lines, Parser.Item item);

    @Override
    public boolean shouldProcess(String typeStr) {
        return this.typeStr.equalsIgnoreCase(typeStr);
    }
    
    protected void removeDefaultTablespace(List<String> lines) {
        int defaultTablespace = searchLineFromEnd(lines, "SET default_tablespace");
        
        if( defaultTablespace > -1 ) 
            lines.set(defaultTablespace, "");
    }}
