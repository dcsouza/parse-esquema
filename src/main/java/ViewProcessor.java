import java.util.List;

public class ViewProcessor extends AbstractFunctionProcessor{
    
    public ViewProcessor( String typeStr ) {
        super( typeStr );
    }

    @Override
    protected boolean shouldIgnore( List<String> lines ) {
        return false;
    }

    @Override
    protected void adjustCode( List<String> lines, Parser.Item item ) {
        changeFromBegin( lines, item.schemaStr, "CREATE VIEW ", "CREATE OR REPLACE VIEW " );
        changeFromEnd( lines, item.schemaStr, "ALTER TABLE ", "ALTER TABLE " );
     }

}
