import java.util.List;

public class TableProcessor extends AbstractFunctionProcessor {

    public TableProcessor(String typeStr) {
        super(typeStr);
    }

    @Override
    protected boolean shouldIgnore(List<String> lines) {
        return false;
    }

    @Override
    protected void adjustCode(List<String> lines, Parser.Item item) {
        changeFromBegin(lines, item.schemaStr, "CREATE TABLE ", "CREATE TABLE ");
        changeFromEnd(lines, item.schemaStr, "ALTER TABLE ", "ALTER TABLE ");

        removeDefaultTablespace(lines);
    }
}
