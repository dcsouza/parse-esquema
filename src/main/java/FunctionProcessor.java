import java.util.List;

public class FunctionProcessor extends AbstractFunctionProcessor {

    public FunctionProcessor( String typeStr ) {
        super( typeStr );
    }

    @Override
    protected void adjustCode( List<String> lines, Parser.Item item ) {
        changeFromBegin( lines, item.schemaStr, "CREATE FUNCTION ", "CREATE OR REPLACE FUNCTION " );
    }

    @Override
    protected boolean shouldIgnore( List<String> lines ) {
        return languageIsC( lines );
    }

    private boolean languageIsC( List<String> lines ) {
        try {
            if( lines.get( 2 ).matches( ".*LANGUAGE c.*" ) )
                return true;

            return false;
        } catch( Exception e ) {
            return false;
        }
    }

}
