import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Parser {

    public static class Item {
        public String nameStr = "";
        public String typeStr = "";
        public String schemaStr = "";
        public String ownerStr = "";
        public String searchPath = "public";

        public static File rootPath;
    }

    /**
     * Interface para plugar processadores. Um processador deve processarum uma
     * seção específica do arquivo de dump: view, table, function etc
     * 
     * @author zimbrao
     * 
     */
    public interface IProcessor {

        /**
         * Indica se deve ou não processar esse objeto já identificado (com o
         * tipo definido). Exemplos de objetos que não são processados:
         * functions em c (que chamam bibliotecas: hstore, postgis etc)
         * 
         * @param typeStr
         * @return
         */
        public boolean shouldProcess(String typeStr);

        /**
         * Faz o processamento da seção.
         * 
         * @param lines
         * @param item
         * @return
         */
        public String doProcess(List<String> lines, Item item);
    }

    private static List<String> linhas;
    private static Item itemAtual = new Item();
    private static Item itemAnterior = new Item();

    private static IProcessor processors[] = { new FunctionProcessor("FUNCTION"), new ViewProcessor("VIEW"),
            new TableProcessor("TABLE"), new TriggerProcessor("TRIGGER"), new ConstraintProcessor("CONSTRAINT"),
            new IndexProcessor("INDEX") };

    public static void main(String[] args) {
        if (args.length == 2 && args[1].length() > 5)
            try {
                Item.rootPath = new File(args[1]);

                system("rm -rf " + Item.rootPath); // Para testes.

                if (Item.rootPath.exists()) {
                    System.out.println("A pasta destino nao deve existir: " + Item.rootPath);
                } else {
                    int inicio = -1, fim = -1;

                    Item.rootPath.mkdir();
                    linhas = Files.readAllLines(Paths.get(args[0]), StandardCharsets.UTF_8);

                    for (int i = 0; i < linhas.size(); i++) {
                        String line = linhas.get(i);

                        if (line.length() > 8 && line.substring(0, 8).equalsIgnoreCase("-- Name:")) {
                            itemAnterior = itemAtual;
                            itemAtual = separaCampos(line);
                            fim = i - 1;

                            if (inicio > 0)
                                processaItem(inicio, fim, itemAnterior);

                            inicio = i + 2;
                        } else if (line.length() > 17 && line.substring(0, 17).equalsIgnoreCase("SET search_path =")) {
                            itemAtual.searchPath = line.replaceAll("([^=]+= )|(, .*)|;", "");
                        }
                    }
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        else {
            System.out.println("Parametros: <dump.sql> path");
        }

    }

    private static void system(String command) {
        try {
            Runtime.getRuntime().exec(command).waitFor();
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    private static void processaItem(int inicio, int fim, Item item) {
        for (IProcessor processor : processors)
            if (processor.shouldProcess(item.typeStr)) {
                String msg = processor.doProcess(linhas.subList(inicio, fim), item);

                if (msg != null)
                    System.out.println(msg);
            }
    }

    private static String valorCampo(String str) {
        String parts[] = str.split(": ");

        return parts.length == 2 ? parts[1].toUpperCase() : "";
    }

    public static Item separaCampos(String line) {
        String campo[] = line.split("; ");
        Item item = new Item();

        if (campo.length >= 1)
            item.nameStr = valorCampo(campo[0]);

        if (campo.length >= 2)
            item.typeStr = valorCampo(campo[1]).toUpperCase();

        if (campo.length >= 3)
            item.schemaStr = valorCampo(campo[2]).toLowerCase();

        if (campo.length >= 4)
            item.ownerStr = valorCampo(campo[3]).toLowerCase();

        // System.out.println( item.nameStr + "," + item.typeStr + "," +
        // item.schemaStr + "," + item.ownerStr );

        return item;
    }
};
