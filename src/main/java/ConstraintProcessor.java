import java.util.List;

public class ConstraintProcessor extends AbstractFunctionProcessor {

    public ConstraintProcessor( String typeStr ) {
        super( typeStr );
    }

    @Override
    protected boolean shouldIgnore( List<String> lines ) {
        return false;
    }

    @Override
    protected void adjustCode( List<String> lines, Parser.Item item ) {
        changeFromBegin( lines, item.schemaStr, "ALTER TABLE ONLY ", "ALTER TABLE ONLY " );
    }
}
