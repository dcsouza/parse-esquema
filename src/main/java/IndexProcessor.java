import java.util.List;

public class IndexProcessor extends AbstractFunctionProcessor {

    public IndexProcessor( String typeStr ) {
        super( typeStr );
    }

    @Override
    protected void adjustCode( List<String> lines, Parser.Item item ) {
        changeInMiddle( lines, item.schemaStr, " ON ", " ON " );
        removeDefaultTablespace(lines);
    }

    @Override
    protected boolean shouldIgnore( List<String> lines ) {
        return languageIsC( lines );
    }

    private boolean languageIsC( List<String> lines ) {
        try {
            if( lines.get( 2 ).matches( ".*LANGUAGE c.*" ) )
                return true;

            return false;
        } catch( Exception e ) {
            return false;
        }
    }

}
